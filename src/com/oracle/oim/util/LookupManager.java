package com.oracle.oim.util;

import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.tcResultSet;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import oracle.iam.catalog.api.CatalogService;
import oracle.iam.conf.api.SystemConfigurationService;
import oracle.iam.configservice.api.Constants;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.platform.OIMClient;
import oracle.iam.provisioning.api.ApplicationInstanceService;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.request.api.RequestService;

/**
 * Clase encargada de ajustar y gestionar Lookups del OIM
 *
 * @author jflautero
 */
public class LookupManager {

    private static final Logger log = Logger.getLogger(LookupManager.class.getName());
//    private static final String OIM_URL = "t3://PRODIDMOIM1.OIM:14000";
    private static final String OIM_URL = "t3://QAIDMOIM1.OIM:14000";
//    private static final String AUTH_CONF = "/Users/jaimoto/git/COL-TEL/authwl.conf";
    private static final String AUTH_CONF = "/Users/Usuario/git/COL-TEL/authwl.conf";
    private static final String APPSERVER_TYPE = "wls";
    private static final String WEBLOGIC_NAME = "oim_server1";
    private static final String OIM_USERNAME = "xelsysadm";
    private static final String OIM_PASSWORD = "Oracle2016";
    private static final String NULL = "(null)";
    private static final String NL = "\n";
    private static final String SP = "\t";

    protected OIMClient _oimClientAuthen = null;
    protected OrganizationManager omgr;
    protected UserManager umgr;
    protected CatalogService csrv;
    protected RequestService rsrv;
    private ApplicationInstanceService aisrv;
    private ProvisioningService psrv;
    private NotificationService nsrv;
    private SystemConfigurationService scsrv;

    public LookupManager() throws LoginException {

        Hashtable<String, String> env = new Hashtable<String, String>();

        System.setProperty("java.security.auth.login.config", AUTH_CONF);
        System.setProperty("APPSERVER_TYPE", APPSERVER_TYPE);
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL, "weblogic.jndi.WLInitialContextFactory");
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, OIM_URL);
        _oimClientAuthen = new OIMClient(env);
        _oimClientAuthen.login(OIM_USERNAME, OIM_PASSWORD.toCharArray());
        //debug("Logged in");

        umgr = _oimClientAuthen.getService(UserManager.class);
        omgr = _oimClientAuthen.getService(OrganizationManager.class);
        csrv = _oimClientAuthen.getService(CatalogService.class);
        rsrv = _oimClientAuthen.getService(RequestService.class);
        aisrv = _oimClientAuthen.getService(ApplicationInstanceService.class);
        psrv = _oimClientAuthen.getService(ProvisioningService.class);
        nsrv = _oimClientAuthen.getService(NotificationService.class);
        scsrv = _oimClientAuthen.getService(SystemConfigurationService.class);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            LookupManager test = new LookupManager();

            //Lookups de Flujos
            test.findLookups("Lookup.ATIS.Configuration");

            //test.findLookups("Lookup.FV.Tipos");
            //test.findLookups("Lookup.WF.GruposAprobadoresEspeciales");
            //test.findLookups("Lookup.WF.AppRoleMetadata");
            //test.findLookups("Lookup.WF.PerfilesEspeciales");
            //test.findLookups("Lookup.WF.Prerequisitos");
            //test.findLookups("Lookup.WF.ManagerGroup");
            //test.removeLookups("Lookup.RNCWF.PlataformaDueno");
            //test.fixLookups();
            //test.findLookups("Lookup.USR_PROCESS_TRIGGERS");
            //test.findLookups("Lookup.ActiveDirectory.Groups");
            //test.copyLookups("Lookup.ATIS.AreasGestion", "Lookup.ATI2.AreasGestion");
            test._oimClientAuthen.logout();
        } catch (Exception ex) {
            Logger.getLogger(LookupManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Encuentra el listado de todos los Lookups del OIM y elimina el prefijo
     * antes del caracter ~
     *
     * @throws Exception
     */
    public void fixLookups() throws Exception {
        System.out.println("iniciando fixLookups ");
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", "*");
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);
        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                    if (lookupDetValue.contains("~")) {
                        int initValor = lookupDetValue.indexOf("~") + 1;
                        int finValor = lookupDetValue.length();
                        String nostart = lookupDetValue.substring(initValor, finValor);
                        Map<String, String> updateMap = new HashMap<String, String>();
                        updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                        updateMap.put("Lookup Definition.Lookup Code Information.Decode", nostart);
                        System.out.println("actualizando " + lookupCode + "/" + lookupDetValue + " -------- " + nostart);
                        lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    /**
     * Elimina el prefijo antes del caracter ~ del Lookup de parametro
     *
     * @throws Exception
     */
    public void fixLookups(String pLookupName) throws Exception {
        System.out.println("iniciando fixLookups " + pLookupName);
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);
        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                System.out.println("iniciando fixLookups " + lookupDetValue);
                if (lookupDetValue.contains("~")) {
                    int initValor = lookupDetValue.indexOf("~") + 1;
                    int finValor = lookupDetValue.length();
                    String nostart = lookupDetValue.substring(initValor, finValor);
                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", nostart);
                    System.out.println("actualizando " + lookupCode + "/" + lookupDetValue + " -------- " + nostart);
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                }
            }

        }
    }

    /**
     * Encuentra el listado del Lookups del OIM y agrega el prefijo del id
     *
     * @throws Exception
     */
    public void addPrefixLookups(String LookupName) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", LookupName);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);
        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                if (lookupDetCode.contains("~")) {
                    int initValor = lookupDetCode.indexOf("~") + 1;
                    int finValor = lookupDetCode.length();
                    String nostart = lookupDetCode.substring(initValor, finValor);
                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", nostart + " - " + lookupDetValue);
                    System.out.println("actualizando " + lookupCode + "/" + lookupDetValue + " -------- " + nostart + " - " + lookupDetValue);
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                } else {
                    break;
                }
            }

        }
    }

    /**
     * Encuentra el listado del Lookups del OIM para y agrega el sufijo con el
     * nombre del Looukup Padre
     *
     * @throws Exception
     */
    public void addSufixLookups(String LookupName, String idlookup, String sufix) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", LookupName);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);
        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");

            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                if (idlookup.equals(lookupDetCode) && !lookupDetValue.contains(sufix)) {
                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue + " [" + sufix + "]");
                    System.out.println("actualizando " + lookupCode + "/" + lookupDetValue + "-" + sufix);
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                    break;
                }
            }
        }
    }

    /**
     * Encuentra el listado del Lookups del OIM para y remueve el primer indice
     *
     * @throws Exception
     */
    public void removeSufixLookups(String pLookupName, String sufix) throws Exception {
        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);

        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                    //System.out.println(lookupDetCode + "\t" + lookupDetValue );
                    int init = lookupDetValue.indexOf("-") + 1;
                    int end = lookupDetValue.length();

                    //System.out.println(" ... " +  );
                    lookupDetValue = lookupDetValue.substring(init, end).trim();

                    Map<String, String> updateMap = new HashMap<String, String>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue);
                    System.out.println("removiendo/actualizando " + lookupCode + "/" + lookupDetValue);
                    lookupOperationsIntf.updateLookupValue(lookupCode, lookupDetCode, updateMap);
                }
            }
        }
    }

    /**
     * Encuentra el Lookups
     *
     * @throws Exception
     */
    public void findLookups(String pLookupName) throws Exception {

        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);

        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                    System.out.println(lookupDetCode + "\t" + lookupDetValue);
                }
            }
        }
        System.out.println("\n\n*********************************************************************************************************************");
    }

    /**
     * Mueve los datos del lookup origen al lookup destino
     *
     * @throws Exception
     */
    private void copyLookups(String pLookupOld, String pLookupNew) throws Exception {

        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupOld);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(pLookupOld);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                Map<String, String> updateMap = new HashMap<>();
                updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue);
                System.out.println("agregando " + pLookupNew + "/" + lookupDetValue + " -------- " + lookupDetValue);
                try {
                    lookupOperationsIntf.addLookupValue(pLookupNew, lookupDetCode, lookupDetValue, "", "");

                } catch (Exception e) {
                    System.err.println("Fallo en " + lookupDetCode + " - " + lookupDetValue);
                }

                // lookupOperationsIntf.updateLookupValue(pLookupNew, lookupDetCode, updateMap);  
            }

        }
    }

    //
    /**
     *
     * @throws Exception
     */
    private void removeLookups(String pLookup) throws Exception {

        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookup);
        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(pLookup);
            for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                tcRDecode.goToRow(j);
                String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                Map<String, String> updateMap = new HashMap<String, String>();
                updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookupDetCode);
                updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookupDetValue);
                System.out.println(" ... checking ... " + lookupDetCode);
                System.out.println("Eliminando " + pLookup + "/" + lookupDetValue + " -------- " + lookupDetValue);
                lookupOperationsIntf.removeLookupValue(pLookup, lookupDetCode);
            }
        }
    }
}
