/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oracle.oim.util.pojo;

/**
 *
 * @author jflautero
 */
public class LookupPojo {
    
    private String psLookupCode;
    private String value;
    private String decode;
    private String lang;
    private String country;

    public LookupPojo(String psLookupCode, String value, String decode) {
        this.psLookupCode = psLookupCode;
        this.value = value;
        this.decode = decode;
    }
    
    public String getPsLookupCode() {
        return psLookupCode;
    }

    public void setPsLookupCode(String psLookupCode) {
        this.psLookupCode = psLookupCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDecode() {
        return decode;
    }

    public void setDecode(String decode) {
        this.decode = decode;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    
    
    
}
