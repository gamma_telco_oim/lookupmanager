package com.oracle.oim.util;

import Thor.API.Operations.tcLookupOperationsIntf;
import com.oracle.oim.util.pojo.LookupPojo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import oracle.iam.catalog.api.CatalogService;
import oracle.iam.conf.api.SystemConfigurationService;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.notification.api.NotificationService;
import oracle.iam.platform.OIMClient;
import oracle.iam.provisioning.api.ApplicationInstanceService;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.request.api.RequestService;

/**
 * Clase utulitaria para cargar y actualizar Lookups en OIM Usar plantilla
 * https://docs.google.com/spreadsheets/d/1S4WW0hmsa5b6hJ8RCOlsiB-ojlu83-qA8GbUJYyVv5U/edit#gid=1182895995
 *
 * @author jflautero
 */
public class LoadLookup {

    private static final Logger log = Logger.getLogger(LoadLookup.class.getName());
    //private static final String OIM_URL = "t3://DEV-IDM:14000";
    //private static final String OIM_URL = "t3://PRODIDMOIM1.OIM:14000";
    private static final String OIM_URL = "t3://QAIDMOIM1.OIM:14000";
//    private static final String AUTH_CONF = "/Users/jaimoto/git/COL-TEL/authwl.conf";
    private static final String AUTH_CONF = "/Users/Usuario/git/COL-TEL/authwl.conf";
    private static final String APPSERVER_TYPE = "wls";
    private static final String WEBLOGIC_NAME = "oim_server1";
    private static final String OIM_USERNAME = "xelsysadm";
    private static final String OIM_PASSWORD = "Oracle2016";
    private static final String NULL = "(null)";
    private static final String NL = "\n";
    private static final String SP = "\t";

    protected OIMClient _oimClientAuthen = null;
    protected OrganizationManager omgr;
    protected UserManager umgr;
    protected CatalogService csrv;
    protected RequestService rsrv;
    private ApplicationInstanceService aisrv;
    private ProvisioningService psrv;
    private NotificationService nsrv;
    private SystemConfigurationService scsrv;

    public LoadLookup() throws LoginException {
        Hashtable<String, String> env = new Hashtable<String, String>();

        System.setProperty("java.security.auth.login.config", AUTH_CONF);
        System.setProperty("APPSERVER_TYPE", APPSERVER_TYPE);
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL, "weblogic.jndi.WLInitialContextFactory");
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, OIM_URL);
        _oimClientAuthen = new OIMClient(env);
        _oimClientAuthen.login(OIM_USERNAME, OIM_PASSWORD.toCharArray());
        //debug("Logged in");

        umgr = _oimClientAuthen.getService(UserManager.class);
        omgr = _oimClientAuthen.getService(OrganizationManager.class);
        csrv = _oimClientAuthen.getService(CatalogService.class);
        rsrv = _oimClientAuthen.getService(RequestService.class);
        aisrv = _oimClientAuthen.getService(ApplicationInstanceService.class);
        psrv = _oimClientAuthen.getService(ProvisioningService.class);
        nsrv = _oimClientAuthen.getService(NotificationService.class);
        scsrv = _oimClientAuthen.getService(SystemConfigurationService.class);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            LoadLookup test = new LoadLookup();
            test.addLookup();
            test._oimClientAuthen.logout();
        } catch (Exception ex) {
            Logger.getLogger(LoadLookup.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo que añade un Lookup
     *
     * @throws Exception
     */
    private void addLookup() throws Exception {

        tcLookupOperationsIntf lookupOperationsIntf = _oimClientAuthen.getService(tcLookupOperationsIntf.class);

        //LOAD
        List<LookupPojo> lookups = loadLookupData();

        for (Iterator<LookupPojo> iterator = lookups.iterator(); iterator.hasNext();) {

            LookupPojo lookup = iterator.next();

            try {

                lookupOperationsIntf.addLookupValue(lookup.getPsLookupCode(), lookup.getValue(), lookup.getDecode(), "", "");
                System.out.println(" OK ");

            } catch (Exception e) {

                try {
                    
                    //PEND FIX UPDATE 
                    Map<String, String> updateMap = new HashMap<>();
                    updateMap.put("Lookup Definition.Lookup Code Information.Code Key", lookup.getValue());
                    updateMap.put("Lookup Definition.Lookup Code Information.Decode", lookup.getDecode());

                    lookupOperationsIntf.updateLookupValue(lookup.getPsLookupCode(), lookup.getValue(), updateMap);

                    System.out.println("Actualización del Lookup realizada.");
                    
                } catch (Exception ex) {
                    
                    System.err.println("Error en addLookup() al intentar actualizar el Lookup: " + ex);
                    
                }
            }
        }
    }

    /**
     * Clase para cargar los lookups, se debe poner el metodo para agregar un
     * lookup, dependeindo de la plantilla
     *
     * @return
     */
    private List<LookupPojo> loadLookupData() {
        List<LookupPojo> listLookups = new ArrayList<LookupPojo>();
        listLookups.add(new LookupPojo("Lookup.ATIS.Configuration",
                "9999",
                "TEST LOOKUP1"));
        return listLookups;
    }
}
